(ns clj-gcp-storage.core
  (:require [clojure.java.io :as io])
  (:import [com.google.cloud.storage
            Storage$BlobTargetOption
            StorageOptions
            BlobId
            BlobInfo
            Blob$BlobSourceOption]
           [com.google.auth.oauth2 ServiceAccountCredentials]
           [java.nio.file Files Paths]
           [org.apache.commons.codec.binary Base64]))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; utilities
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defn- get-blob-info [content-type bucket & {:keys [name] :or {name nil}}]
  (let [blob-id (BlobId/of bucket (or name (str (java.util.UUID/randomUUID))))
        builder (doto
                  (BlobInfo/newBuilder blob-id)
                  (.setContentType content-type))]
    (.build builder)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; api
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defn get-storage [project]
  (-> (StorageOptions/newBuilder)
      (.setProjectId project)
      (.build)
      (.getService)))

(defn upload-bytes [storage bucket byte-arr content-type & {:keys [name] :or {name nil}}]
  (let [blob-info (get-blob-info content-type bucket :name name)]
    (.create storage
             blob-info
             byte-arr
             (into-array Storage$BlobTargetOption []))))

(defn upload-uri [storage bucket uri content-type & {:keys [name] :or {name nil}}]
  (let [path (Paths/get uri)
        byte-arr (Files/readAllBytes path)]
    (upload-bytes storage bucket byte-arr content-type :name name)))

(defn delete-blob [storage bucket blob-id]
  (let [bid (BlobId/of bucket blob-id)]
    (.delete storage bid)))

(defn get-blob [storage bucket blob-name]
  (let [bid (BlobId/of bucket blob-name)]
    (.getContent (.get storage bid) (into-array Blob$BlobSourceOption []))))

(defn blob->base64 [blob]
  (when blob (Base64/encodeBase64 blob)))

(defn download-blob [storage bucket blob-name path]
  (let [bid (BlobId/of bucket blob-name)]
    (.downloadTo (.get storage bid) path)))
