(defproject org.clojars.s312569/clj-gcp-storage "0.1.1"
  :description "Library for interacting with Google's cloud storage service."

  :url "https://gitlab.com/jmpublic/clj-gcp-storage"

  :license {:name "EPL-2.0 OR GPL-2.0-or-later WITH Classpath-exception-2.0"
            :url "https://www.eclipse.org/legal/epl-2.0/"}

  :dependencies [[org.clojure/clojure "1.10.1"]
                 [com.google.http-client/google-http-client "1.39.2"]
                 [com.google.api-client/google-api-client "1.31.5"]
                 [com.google.http-client/google-http-client-gson "1.39.2"]
                 [com.google.cloud/google-cloud-storage "1.115.0"]
                 [com.google.auth/google-auth-library-oauth2-http "0.26.0"]]

  :repl-options {:init-ns clj-gcp-storage.core})
